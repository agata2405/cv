# Curriculum Vitae -EDYCJA PRZEZ BITBUCKET-
## Agata Barańska

#### Dane kontaktowe
|||
| ------ | ------ |
| Numer telefonu: | +48 123-456-789 |
| Adres e-mail: | mail@poczta.pl |
| Adres korespondencyjny: | ul. Przykładowa, 12-345 Lublin |

#### O mnie:
>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus blandit ac tortor nec condimentum. Nulla quam erat, volutpat non arcu quis, pharetra porta tellus. Aliquam fringilla vitae neque vitae mattis. Donec sed laoreet nulla, quis imperdiet tellus. Vestibulum vitae libero vitae magna viverra iaculis. Nunc feugiat, nunc at imperdiet consequat, risus lorem gravida nisl, vitae volutpat augue nulla a mi.
#
__________________
### Doświadczenie zawodowe:
###### 08/2015 - 09/2018
Firma Usługowo-Handlowa "XYZ", Poznań
Stanowisko: asystentka biura i rekrutacji

Najważniejsze obowiązki:

- wsparcie kadry kierowniczej po przez planowanie logistyczne wyjazdów służbowych, rozliczanie kosztów delegacji, umawianie i organizacja zebrań służbowych, prowadzenie i pilnowanie kalendarza spotkań;
- bieżąca administracja w biurze m.in. prowadzenie dokumentacji, archiwizacja dokumentów, koordynacja przepływu dokumentów;
- prowadzenie rozmów telefonicznych i korespondencji elektronicznej w języku angielskim z międzynarodowym zespołem (pracownikami firmy) i kontrahentami firmy.


### Wykształcenie:
###### 10/2019 - 10/2021
Uniwersytet im. Marii Curie Skłodowskiej w Lublinie
Kierunenek: Geoinformatyka
Stopień: Studia magisterskie


### Umiejętności:
* dobre umiejętności interpersonalne i komunikatywność;
* wysoka kultura osobista;
* zdolności organizacyjne i umiejętność oznaczenia priorytetów;
* znajomość języka angielskiego na poziomie C1;
* bardzo dobra obsługa komputera i oprogramowania: "eArchiwista", "Kalendarz Google" i inne.;
* MS Excel - średnio zaawansowany (tabele przestawne, wyszukiwanie i sortowanie, budowanie formuł);
* MS Word - zaawansowany;
* MS PowerPoint - średnio zaawansowany;
* czynne Prawo Jazdy kat. B;