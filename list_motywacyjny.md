# List motywacyjny

Agata Barańska
Tel. 123-456-789
e-mail: agata@poczta.pl
Lublin, 13 listopada 2020 r.


Szanowni Państwo,

w odpowiedzi na ogłoszenie dotyczące stanowiska specjalisty ds. marketingu przedstawiam swoją kandydaturę, która, mam nadzieję, spotka się z Państwa zainteresowaniem.
Do ubiegania się o pracę na powyższym stanowisku skłoniły mnie możliwości wykorzystania posiadanego doświadczenia w branży FMCG, a także umiejętności zdobyte w działach marketingu i obsługi klienta.

W ramach realizowanych projektów miałem okazję uczestniczyć we wprowadzaniu nowych marek na rynek, tworzyć i realizować strategie marketingowe. Dzięki działaniom zespołu, w którym pracuję, firma X w ciągu roku potroiła sprzedaż swoich produktów w Polsce.

Mam również doświadczenie w samodzielnym prowadzeniu kompleksowych kampanii marketingowych w internecie (reklamy banerowe, Google AdWords, mailingi). Miałem również okazję uczestniczyć w budowaniu programów lojalnościowych i tworzeniu ofert dla firmy Y.

Zachęcam do zapoznania się z moim CV, w którym zawarłem opis mojego doświadczenia zawodowego i kwalifikacji.

Mam nadzieję, że zaproszą mnie Państwo na spotkanie, podczas którego będę mógł szerzej przedstawić swoje doświadczenia i umiejętności oraz dokładniej zapoznać się z Państwa oczekiwaniami.

Z wyrazami szacunku,
Agata Barańska